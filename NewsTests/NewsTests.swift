//
//  NewsTests.swift
//  NewsTests
//
//  Created by MAC12166 on 6/6/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import XCTest
@testable import News

class NewsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testNumberOfData() {
        let data = NewsListDataManager.init(using: "data")
        let result = data.retrieveNewsList()
        
        XCTAssert(result.count == 6, "The data of the file is incorect!")
    }

    func testMemberDataFromDataManager(){
        let data = NewsListDataManager.init(using: "data")
        let result = data.retrieveNewsList()
        
        guard let firstMember = result.first else{
            XCTFail("The first member is not exists")
            return
        }
        
        XCTAssert(firstMember.title != nil, "The title is not exists!")
        XCTAssert(firstMember.content1 != nil, "The content1 is not exists!")
        XCTAssert(firstMember.content2 != nil, "The content2 is not exists!")
        XCTAssert(firstMember.author != nil, "The author is not exists!")
        XCTAssert(firstMember.imageUrl != nil, "The imageUrl is not exists!")
        XCTAssert(firstMember.time != nil, "The time is not exists!")
    }
    
    func testInteractor(){
        let interactor = NewsListInteractor.init()
        let data = interactor.retrieveNewsList()
        XCTAssert(data.count == 6, "count - fail")
        
        guard let first = data.first else{
            XCTFail("First is not exists!")
            return
        }
        
        XCTAssert(!first.author.isEmpty, "Author empty")
    }
    
    func testMemberDataFromInteractor(){
        let interactor = NewsListInteractor.init()
        let result = interactor.retrieveNewsList()
        
        guard let firstMember = result.first else{
            XCTFail("The first member is not exists")
            return
        }
        
        XCTAssert(!firstMember.title.isEmpty, "The title is not exists!")
        XCTAssert(!firstMember.content1.isEmpty, "The content1 is not exists!")
        XCTAssert(!firstMember.content2.isEmpty, "The content2 is not exists!")
        XCTAssert(!firstMember.author.isEmpty, "The author is not exists!")
        XCTAssert(!firstMember.imageUrl.isEmpty, "The imageUrl is not exists!")
    }
    
    func testPresenter(){
        let presenter = NewsListPresenter()
        let view = NewsTableViewController()
        presenter.view = view
        
        XCTAssert(presenter.loadView() == true, "load fail")
    }
    
    func testRouter(){
        guard let navigation = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NavigationTable") as? UINavigationController else{
            XCTFail("Could not get the navigation!")
            return
        }
        let router = NewsListRouter()
        guard let view = navigation.visibleViewController else{
            XCTFail("Could not get the view")
            return
        }
        
        router.presentNewsDetail(fromView: view, forNews: NewsModel())
        
        guard let numberOfViewController = view.navigationController?.viewControllers.count else {
            XCTFail("AccessibilityElementCount not exists!")
            return
        }
        
        XCTAssert(numberOfViewController > 0, "fail")
    }
    
   
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
