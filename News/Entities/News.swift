//
//  News.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation

public class News: Decodable{
    var title: String?
    var content1: String?
    var content2: String?
    var imageUrl: String?
    var author: String?
    var time: String?
}
