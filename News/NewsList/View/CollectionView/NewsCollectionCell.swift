//
//  NewsCollectionCell.swift
//  News
//
//  Created by MAC12166 on 6/18/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import UIKit

class NewsCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!

    var presenter: NewsListPresenterProtocol?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    func update(_ news: NewsModel){
        
        self.widthConstraint.constant = UIScreen.main.bounds.width
        self.txtTitle.text = news.title
        
        if let url = URL(string: news.imageUrl), let placeholderImage = UIImage(named: "placeholder"){
            imageCell.af_setImage(withURL: url, placeholderImage: placeholderImage)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        self.txtTime.text = news.author + " " + news.parseTime()
    }
    
}
