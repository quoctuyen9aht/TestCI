//
//  NewsListView.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import UIKit
import PKHUD

class NewsTableViewController: UITableViewController{

    var presenter: NewsListPresenterProtocol?
    var newsList: [NewsModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 135
        
        presenter = NewsListPresenter()
        presenter?.view = self
        
        guard let isLoaded = presenter?.loadView() else {
            return
        }
        
        if !isLoaded {
            HUD.flash(.label("Could not load the view!"), delay: 2.0)
        }
    }
}

extension NewsTableViewController: NewsListViewProtocol{
    func showAll(_ newsList: [NewsModel]){
        if newsList.count == 0{
            HUD.flash(.label("The file is empty"), delay: 2.0)
            return
        }
        self.newsList = newsList
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell else{
            print("Could not dequeue reusablecell!")
            return UITableViewCell()
        }
        
        cell.update(newsList[indexPath.row])
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showDetail(news: newsList[indexPath.row])
    }
    
    
}
