//
//  NewsTableViewCell.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var txtTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let colorView = UIView()
        colorView.backgroundColor = UIColor.lightGray
        
        self.selectedBackgroundView = colorView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(_ news: NewsModel){
        self.txtTitle.text = news.title
        
        if let url = URL(string: news.imageUrl), let placeholderImage = UIImage(named: "placeholder"){
            titleImage.af_setImage(withURL: url, placeholderImage: placeholderImage)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        self.txtTime.text = news.author + " " + news.parseTime()
    }
}
