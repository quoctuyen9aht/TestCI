//
//  NewsListRoutingProtocol.swift
//  News
//
//  Created by QuocTuyen on 6/22/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit

protocol NewsListRouterProtocol {
    func presentNewsDetail(fromView view: UIViewController, forNews news: NewsModel)
}
