//
//  NewsListViewProtocols.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
protocol NewsListViewProtocol {
    var presenter: NewsListPresenterProtocol? { get set }
    var newsList: [NewsModel] { get set }
    func showAll(_ newsList: [NewsModel])
}
