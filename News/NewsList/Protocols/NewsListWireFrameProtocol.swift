//
//  NewsListWireFrameProtocol.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit

protocol NewsListWireFrameProtocol{
    func presentNewsDetail(fromView view: UIViewController, forNews news: NewsModel)
}

