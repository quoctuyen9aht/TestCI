//
//  NewsListRouting.swift
//  News
//
//  Created by QuocTuyen on 6/22/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit
class NewsListRouter: NewsListRouterProtocol {
    func presentNewsDetail(fromView view: UIViewController, forNews news: NewsModel) {
        let newsDetailView = NewsDetailWireFrame.createNewsDetailModule(forNews: news)
        view.navigationController?.pushViewController(newsDetailView, animated: true)
    }
}
