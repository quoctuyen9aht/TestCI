//
//  NewsDetailWireFrame.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
import UIKit
class NewsDetailWireFrame:NewsDetailWireFrameProtocol{
    static func createNewsDetailModule(forNews news: NewsModel) -> UIViewController {
        if let view = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailView") as? NewsDetailView{
            let presenter = NewsDetailPresenter()
            view.presenter = presenter
            presenter.view = view
            presenter.news = news
            return view
        } else{
            return UIViewController()
        }
    }
    
    static var mainStoryboard: UIStoryboard{
         return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
