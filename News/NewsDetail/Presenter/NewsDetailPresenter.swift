//
//  NewsDetailPresenter.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
class NewsDetailPresenter: NewsDetailPresenterProtocol{
    var view: NewsDetailViewProtocol?
    var news: NewsModel?
    
    func loadView() {
        if let newsModel = news{
            view?.showDetail(newsModel)
        }
    }
    
}
