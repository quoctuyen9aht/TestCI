//
//  NewsDetailPresenterProtocol.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
protocol NewsDetailPresenterProtocol {
    var view: NewsDetailViewProtocol? { get set }
    var news: NewsModel? { get set }
    func loadView()
}
