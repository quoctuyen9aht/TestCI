//
//  NewsDetailViewProtocol.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import Foundation
protocol NewsDetailViewProtocol {
    func showDetail(_ news: NewsModel)
}
