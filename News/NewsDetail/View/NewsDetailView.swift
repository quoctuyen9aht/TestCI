//
//  NewsDetailView.swift
//  News
//
//  Created by MAC12166 on 6/7/18.
//  Copyright © 2018 MAC12166. All rights reserved.
//

import UIKit

class NewsDetailView: UIViewController {
   
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var txtContent1: UILabel!
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var txtContent2: UILabel!

    
    var presenter: NewsDetailPresenterProtocol?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.loadView()
    }
}

extension NewsDetailView: NewsDetailViewProtocol{
    func showDetail(_ news: NewsModel) {
        self.txtTitle.text = news.title
        self.txtTime.text = news.author + " " + news.parseTime()
        self.txtContent1.text = news.content1
        
        if let url = URL(string: news.imageUrl), let placeholderImage = UIImage(named: "placeholder"){
            imageDetail.af_setImage(withURL: url, placeholderImage: placeholderImage)
        }
        
        txtContent2.text = news.content2
    }
}
